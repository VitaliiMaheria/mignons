﻿(function (angular) {
    'use strict';

    function OrdersService($http) {
        var service = {};
        service.postOrder = postOrder;
        return service;

        function postOrder(newOrder, callback) {
            $http.post('Orders/MoreDetails', {
                params: {
                    newOrder: {
                        Email: newOrder.email,
                        Quantity: newOrder.quantity,
                        Name: newOrder.name,
                        Phone: newOrder.phone
                    }
                }
            })
                .success(function (response) {
                    callback(null, response);
                })
                .error(function (e) {
                    callback(e);
                });
        }
    }

    OrdersService.$inject = ['$http'];
    angular
        .module('OrdersModule')
        .service('OrdersService', OrdersService);

})(window.angular);
