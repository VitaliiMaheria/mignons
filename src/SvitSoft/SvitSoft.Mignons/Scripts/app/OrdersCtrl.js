﻿(function (angular) {
    'use strict';

    function OrdersCtrl($scope, $http, UsersService, OrdersService) {

        var vm = this;
        vm.order = {
            quantity: 1,
            email: 'email@domain.com',
            name: "",
            phone:""
        };
        vm.checkUser = checkUser;
        
        function checkUser() {

            window.location.href = "Orders/MoreDetails?email=" + vm.order.email + "&quantity=" + vm.order.quantity;


            //UsersService.getUserByEmail(vm.order.email,
            //    function (error, answer) {
            //        if (!error) {
            //            if (!answer) {
            //                window.location.href = "Orders/MoreDetails?email=" + vm.order.email + "&quantity=" + vm.order.quantity;
            //            }
            //            else {
            //                vm.order.name = answer.Name;
            //                vm.order.phone = answer.Phone;
            //                OrdersService.postOrder(vm.order, function (error, answer) {
            //                    if (!error) {
            //                        window.location.href = "Orders/OrdersList";
            //                    };

            //                });
            //            }
            //        }
            //    });
        }

    }

    OrdersCtrl.$inject = [
        '$scope',
        '$http',
        'UsersService',
        'OrdersService'
    ];

    angular.module('OrdersModule')
        .controller('OrdersCtrl', OrdersCtrl);
})(window.angular);
