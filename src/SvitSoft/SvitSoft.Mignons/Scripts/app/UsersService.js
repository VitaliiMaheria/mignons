﻿(function (angular) {
    'use strict';

    function UsersService($http) {
        var service = {};
        service.getUserByEmail = getUserByEmail;
        return service;

        function getUserByEmail(email, callback) {
            $http.get('Users/GetUserByEmail', {
                params: {
                    email: email
                }
            })
                .success(function (response) {
                    callback(null, response);
                })
                .error(function (e) {
                    callback(e);
                });
        }

    }

    UsersService.$inject = ['$http'];
    angular
        .module('OrdersModule')
        .service('UsersService', UsersService);

})(window.angular);
