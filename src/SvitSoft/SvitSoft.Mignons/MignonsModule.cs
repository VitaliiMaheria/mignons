﻿using Autofac;
using SharpRepository.EfRepository;
using SharpRepository.Repository;
using SvitSoft.Core.Common;
using SvitSoft.Core.Domain;
using SvitSoft.Core.Domain.Models;
using SvitSoft.DataAccess.Contexts;
using SvitSoft.DataAccess.Repositories;
using SvitSoft.Mignons.Core.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvitSoft.Mignons
{
    public class MignonsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ShopContext>()
                .As<DbContext>()
                .AsSelf()
                .WithParameter("nameOrConnectionString", ConnectionStringsProvider.GetShopConnectionString())
                .InstancePerRequest();

            builder.RegisterType<OrderLinesRepository>()
                .As<IOrderLinesRepository>()
                .InstancePerRequest();

            builder.RegisterType<UsersRepository>()
                .As<IUsersRepository>()
                .InstancePerRequest();

            builder.RegisterType<ProductsRepository>()
                .As<IProductsRepository>()
                .InstancePerRequest();

            builder.RegisterType<OrdersService>()
                .AsSelf()
                .InstancePerRequest();

            builder.RegisterType<UsersService>()
                .AsSelf()
                .InstancePerRequest();
        }
    }
}
