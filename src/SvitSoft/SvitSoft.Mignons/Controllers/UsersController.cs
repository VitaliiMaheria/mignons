﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SvitSoft.Mignons.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SvitSoft.Mignons.Controllers
{
    public class UsersController : Controller
    {
        private readonly UsersService _usersService;

        public UsersController(UsersService usersService)
        {
            _usersService = usersService;
        }

        // GET: Users
        public JsonResult GetUserByEmail(string email)
        {
            return Json(_usersService.FindByEmail(email), JsonRequestBehavior.AllowGet);


            //var camelCaseFormatter = new JsonSerializerSettings();
            //camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //return Json(
            //    JsonConvert.SerializeObject(_usersService.FindByEmail(email), camelCaseFormatter),
            //    JsonRequestBehavior.AllowGet);

     //       return Json(JsonConvert.SerializeObject(
     //_usersService.FindByEmail(email),
     //new JsonSerializerSettings()
     //{
     //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
     //}), JsonRequestBehavior.AllowGet);

        }
    }
}