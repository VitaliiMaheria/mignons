﻿using SvitSoft.Core.Domain.Models;
using SvitSoft.Mignons.Core.Models;
using SvitSoft.Mignons.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SvitSoft.Mignons.Controllers
{
    public class OrdersController : Controller
    {

        private readonly OrdersService _ordersService;
        private readonly UsersService _usersService;
        public OrdersController(OrdersService ordersService, UsersService usersService)
        {
            _ordersService = ordersService;
            _usersService = usersService;
        }

        [HttpGet]
        public ActionResult NewOrder()
        {
            return View();
        }

        [HttpGet]
        public ActionResult MoreDetails(string email, double quantity)
        {
            var newOrder = new NewOrderModel { Email = email, Quantity = quantity };
            var user = _usersService.FindByEmail(email);
            if(user!=null)
            {
                newOrder.Name = user.Name;
                newOrder.Phone = user.Phone;
                _ordersService.CreateOrder(newOrder, user.Id);
                return RedirectToAction("OrdersList");
            }

            return View();
        }

        [HttpPost]
        public ActionResult MoreDetails(NewOrderModel newOrder)
        {
            if (!ModelState.IsValid)
                return View(newOrder);

            var userId = _usersService.CreateIfNotExists(new User
                            {
                                Name = newOrder.Name,
                                Email = newOrder.Email,
                                Phone = newOrder.Phone
                            });
            _ordersService.CreateOrder(newOrder, userId);

            return RedirectToAction("OrdersList");
        }

        public ActionResult OrdersList(string productName = "Mignon")
        {
            var orders = _ordersService.GetOrdersStory(productName);

            return View(orders);
        }
    }
}