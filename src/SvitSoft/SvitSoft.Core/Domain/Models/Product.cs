﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvitSoft.Core.Domain.Models
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<OrderLine> OrderLines { get; set; }

        public Product()
        {
            OrderLines = new List<OrderLine>();
        }
    }
}
