﻿using SharpRepository.Repository;
using SvitSoft.Core.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvitSoft.Core.Domain
{
    public interface IUsersRepository: IRepository<User, Guid>
    {
    }
}
