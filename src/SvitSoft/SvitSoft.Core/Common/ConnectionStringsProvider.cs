﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvitSoft.Core.Common
{
    public class ConnectionStringsProvider
    {
        public static string GetShopConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[Constants.ConnectionName.ShopConnection].ConnectionString;
        }

        private static class Constants
        {
            public static class ConnectionName
            {
                public const string ShopConnection = "ShopConnection";
            }
        }
    }
}