﻿using SvitSoft.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SvitSoft.Core.Domain.Models;

namespace SvitSoft.Mignons.Core.Services
{
    public class UsersService
    {
        private readonly IUsersRepository _usersRepository;

        public UsersService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public Guid CreateIfNotExists(User user)
        {
            var userEntity = FindByEmail(user.Email);
            if (userEntity == null)
            {
                user.Id = Guid.NewGuid();
                _usersRepository.Add(user);
                return user.Id;
            }
            return userEntity.Id;
        }

        public User FindByEmail(string email)
        {
            return _usersRepository.Find(u => u.Email == email);
        }

    }
}
