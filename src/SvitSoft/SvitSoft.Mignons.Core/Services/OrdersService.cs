﻿using SharpRepository.Repository.FetchStrategies;
using SharpRepository.Repository.Specifications;
using SvitSoft.Core.Domain;
using SvitSoft.Core.Domain.Models;
using SvitSoft.Mignons.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvitSoft.Mignons.Core.Services
{
    public class OrdersService
    {
        private readonly IOrderLinesRepository _orderLinesRepository;
        private readonly IProductsRepository _productsRepository;
        private readonly IUsersRepository _usersRepository;

        public OrdersService(
            IOrderLinesRepository orderLinesRepository,
            IProductsRepository productsRepository,
        IUsersRepository usersRepository
        )
        {
            _orderLinesRepository = orderLinesRepository;
            _productsRepository = productsRepository;
            _usersRepository = usersRepository;
        }

        public IEnumerable<OrderModel> GetOrdersStory(string productName)
        {
            var strategy = new GenericFetchStrategy<OrderLine>();
            strategy.Include(ol => ol.Order)
                .Include(ol => ol.Order.User)
                .Include(ol => ol.Product);

            var spec = new Specification<OrderLine>(ol=>ol.Product.Name == productName) { FetchStrategy = strategy };

            var orders = _orderLinesRepository
                .FindAll(spec)
                .GroupBy(ol => ol.Order.User.Email).ToList()
                .Select(o =>
                {
                    var user = o.First().Order.User;
                    return new OrderModel
                    {
                        UserName = user.Name,
                        ProductName = productName,
                        LastOrder = o.OrderBy(ol => ol.Order.OrderDate).Last().Order.OrderDate,
                        OrderCount = o.Count(),
                        ProductCount = o.Sum(or => or.Quantity)
                    };
                }).OrderByDescending(ol=>ol.LastOrder);
            return orders;
        }

        public void CreateOrder(NewOrderModel newOrder, Guid userId, string productName = "Mignon")
        {
            var productId = _productsRepository.Find(p => p.Name == productName).Id;
            var order = new Order {
                Id = Guid.NewGuid(),
                OrderDate = DateTime.Now,
                UserId = userId
            };

            var orderLine = new OrderLine
            {
                ProductId = productId,
                Quantity = newOrder.Quantity,
                Order = order
            };

            _orderLinesRepository.Add(orderLine);

        }
    }
}
