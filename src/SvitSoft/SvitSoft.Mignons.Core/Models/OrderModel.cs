﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvitSoft.Mignons.Core.Models
{
    public class OrderModel
    {
        public string UserName { get; set; }
        public string ProductName { get; set; }
        public DateTime LastOrder { get; set; }
        public int OrderCount { get; set; }
        public double ProductCount { get; set; }
    }
}
