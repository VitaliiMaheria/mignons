﻿using SvitSoft.Core.Domain.Models;
using System;
using System.Data.Entity;

namespace SvitSoft.DataAccess.Contexts
{
    public class ShopContextInitializer : DropCreateDatabaseAlways<ShopContext>
    {
        protected override void Seed(ShopContext context)
        {
            base.Seed(context);

            var product1 = new Product { Id = Guid.NewGuid(), Name = "Mignon" };

            var user1 = new User { Id = Guid.NewGuid(), Name = "User1", Email = "user1@gmail.com" };
            var user2 = new User { Id = Guid.NewGuid(), Name = "User2", Email = "user2@gmail.com" };

            var order1 = new Order { Id = Guid.NewGuid(), UserId = user1.Id , OrderDate = DateTime.Now.AddDays(-1)};
            var order2 = new Order { Id = Guid.NewGuid(), UserId = user1.Id, OrderDate = DateTime.Now };
            var order3 = new Order { Id = Guid.NewGuid(), UserId = user2.Id, OrderDate = DateTime.Now.AddYears(-1) };
            var order4 = new Order { Id = Guid.NewGuid(), UserId = user2.Id, OrderDate = DateTime.Now.AddDays(-10).AddHours(1).AddMinutes(23) };

            var orderLine1 = new OrderLine { Id = Guid.NewGuid(), ProductId = product1.Id, OrderId = order1.Id, Quantity = 5 };
            var orderLine2 = new OrderLine { Id = Guid.NewGuid(), ProductId = product1.Id, OrderId = order2.Id, Quantity = 10 };
            var orderLine3 = new OrderLine { Id = Guid.NewGuid(), ProductId = product1.Id, OrderId = order3.Id, Quantity = 15 };
            var orderLine4 = new OrderLine { Id = Guid.NewGuid(), ProductId = product1.Id, OrderId = order4.Id, Quantity = 20 };

            context.Products.Add(product1);
            context.Users.Add(user1);
            context.Users.Add(user2);
            context.Orders.Add(order1);
            context.Orders.Add(order2);
            context.Orders.Add(order3);
            context.Orders.Add(order4);
            context.OrderLines.Add(orderLine1);
            context.OrderLines.Add(orderLine2);
            context.OrderLines.Add(orderLine3);
            context.OrderLines.Add(orderLine4);
            context.SaveChanges();

        }
    }
}