﻿using SharpRepository.EfRepository;
using SvitSoft.Core.Domain;
using SvitSoft.Core.Domain.Models;
using SvitSoft.DataAccess.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvitSoft.DataAccess.Repositories
{
    public class ProductsRepository: EfRepository<Product, Guid>, IProductsRepository
    {
        public ProductsRepository(ShopContext context): base(context)
        {
        }
    }
}
